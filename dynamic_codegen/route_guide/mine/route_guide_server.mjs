import * as grpc from '@grpc/grpc-js'
import * as protoLoader from '@grpc/proto-loader'
import { dirname } from 'path';
import { fileURLToPath } from 'url';
import * as _ from 'lodash'

const __dirname = dirname(fileURLToPath(import.meta.url));
const PROTO_PATH = `${__dirname}/../../../protos/mine/route.proto`

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
})

const routeGuide = grpc.loadPackageDefinition(packageDefinition).routeGuide;
console.info({routeGuide})

const server = new grpc.Server()

const feature_list = []

/**
 * Get a feature object at the given point, or creates one if it does not exist.
 * @param {point} point The point to check
 * @return {feature} The feature object at the point. Note that an empty name
 *     indicates no feature
 */
const checkFeature = (point) => {
  const candidates = feature_list
    .filter(feature => feature.location.latitude === point.latitude && feature.location.longitude === point.longitude)
    .map(feature => feature.name)
  
  return candidates.length !== 0
    ? candidates[0]
    : ({
      name: '',
      location: point
    })
}

function getFeature(call, callback) {
  callback(null, checkFeature(call.request));
}

function listFeatures(call) {
  var lo = call.request.lo;
  var hi = call.request.hi;
  var left = _.min([lo.longitude, hi.longitude]);
  var right = _.max([lo.longitude, hi.longitude]);
  var top = _.max([lo.latitude, hi.latitude]);
  var bottom = _.min([lo.latitude, hi.latitude]);
  // For each feature, check if it is in the given bounding box
  _.each(feature_list, function(feature) {
    if (feature.name === '') {
      return;
    }
    if (feature.location.longitude >= left &&
        feature.location.longitude <= right &&
        feature.location.latitude >= bottom &&
        feature.location.latitude <= top) {
      call.write(feature);
    }
  });
  call.end();
}